/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #8                 *
 * Due Date: April 15th, 2019             *
 * Last Modified: April 11th, 2019        *
 ******************************************/

#include <iostream>
#include <fstream>
#include "Appdata.h"

using namespace std;

/***********************
 * Class Name: Queue
 * Purpose: Establish a Queue data structure and methods
 *     to modify it
 * Last Modified: April 12th
 ***********************/
#ifndef QUEUE
#define QUEUE

class Queue
{
    public:
		Queue();
		~Queue();
		void enqueue(Appdata newData);
		Appdata dequeue();
		bool isEmpty();
		void printAll();
		friend ostream& operator <<(ostream& outs, const Queue& theQueue);

    private:
		struct QNode
		{
		    Appdata data;
		    QNode *next;
		};
		QNode *front;
		QNode *back;
};
#endif
