/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #7                 *
 * Due Date: April 4th, 2019              *
 * Last Modified: March 29th, 2019        *
 ******************************************/

#include <iostream>
#include <fstream>
#include "Appdata.h"

using namespace std;

/***********************
 * Class Name: Stack
 * Purpose: Establish a stack data structure and methods
 *     to modify it
 * Last Modified: April 4th, 2019
 ***********************/
#ifndef STACK
#define STACK

class Stack
{
    public:
	Stack();
	~Stack();
	void push(Appdata newData);
	Appdata pop();
	bool isEmpty();
	Appdata getTop();
	void printStack();
	friend ostream& operator <<(ostream& outs, const Stack& theStack);

    private:
	struct StackNode
	{
	    Appdata data;
	    StackNode *next;
	};
	StackNode *top;
};
#endif
