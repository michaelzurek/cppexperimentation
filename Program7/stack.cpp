/******************************************
 * Name: Michael Zurek, Christopher Smith *
 * Class: CSCI 121, Spring 2019           *
 * Assignment: Program #7                 *
 * Due Date: April 4th, 2019              *
 * Last Modified: April 4th, 2019         *
 ******************************************/

#include "stack.h"

Stack::Stack()
{
    top = NULL;
}

Stack::~Stack()
{
    while (!isEmpty())
    {
	    pop();
    }
}
/***********************************************************
 * Function: push
 * Author: Michael Zurek and Christopher Smith
 * Purpose: To place data inside of a stack data structure
 * Parameters: newData, of type Appdata
 * Return Value: Nothing
 * Last Modified: April 4th, 16:00
 **********************************************************/
void Stack::push(Appdata newData)
{
    StackNode *temp;

    if (isEmpty())
    {
        // Empty stack case
        top = new(StackNode);
        top -> data = newData;
        top -> next = NULL;

    }
    else
    {
        // Normal
	    temp = new(StackNode);
	    temp -> data = newData;
        temp -> next = top;
        top = temp;   
    }
}
/***********************************************************
 * Function: pop
 * Author: Michael Zurek and Christopher Smith
 * Purpose: To remove data from the top of the stack structure
 * Parameters: None
 * Return Value: Data of type Appdata
 * Last Modified: April 4th, 16:01
 **********************************************************/
Appdata Stack::pop()
{
    Appdata temp;
    StackNode *delPtr = top;

    if (isEmpty())
    {
	    cout << "ERROR: Empty stack cannot be pop'ed\n";
    }
    else
    {
        temp = top->data;
        top = top->next;
        delete(delPtr);
    }

    return(temp);
}
/***********************************************************
 * Function: isEmpty
 * Author: Christopher Smith
 * Purpose: To determine if the stack structure is empty
 * Parameters: None
 * Return Value: Boolean value
 * Last Modified: April 4th, 16:03
 **********************************************************/
bool Stack::isEmpty()
{
    return(top == NULL);
}
/***********************************************************
 * Function: getTop
 * Author: Michael Zurek and Christopher Smith
 * Purpose: To grab data at the top of the stack structure
 * Parameters: None
 * Return Value: Data of type appdata
 * Last Modified: April 4th, 16:04
 **********************************************************/
Appdata Stack::getTop()
{
    Appdata temp;

    temp = top -> data;

    return(temp);
}
/***********************************************************
 * Function: printStack
 * Author: Michael Zurek and Christopher Smith
 * Purpose: To print all data in the stack structure to the screen
 * Parameters: None
 * Return Value: None
 * Last Modified: April 4th, 16:05
 **********************************************************/
void Stack::printStack()
{
    StackNode * temp = top;

    cout << "Stack contents:\n";

    while (temp != NULL)
    {
        cout << "\t" << temp -> data << endl;
        temp = temp -> next;
    }
    cout << "Done.\n";
}

ostream& operator <<(ostream& outs, const Stack& theStack)
{
    Stack::StackNode *temp = theStack.top;

    outs << "Stack contents:\n";
    while (temp != NULL)
    {
	outs << "\t" << temp->data << endl;
	temp = temp->next;
    }
    outs << "Done.\n";

    return(outs);
}


