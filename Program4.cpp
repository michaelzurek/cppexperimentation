/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 121, Spring 2019         *
 * Assignment: Program 4                *
 * Due Date:  February 18th, 2019       *
 * Last Modified: February 17th, 14:25  *
 ****************************************/

#include <iostream>
#include <fstream>

// Per program specifications, there will be 4 tests per student.
#define numTests 4

using namespace std;

// Average tests for a student
float testAvg(int num1, int num2, int num3, int num4)
{
    return((float) (num1+num2+num3+num4)/4.0);
}
// Average scores for a particular test across the class
// taking in the amount of students, the test scores, and quiz
float classAvg(int maxStudents,const int testScores[][numTests], 
int testNum)
{
    int student, sum = 0;
    for (student = 0; student < maxStudents; student++)
    {
        sum = sum + testScores[student][testNum];
    }
    return(sum/(float) maxStudents);
}

int main() 
{
    int maxStudents;
    // Open the file that contains the grades
    ifstream inStream("grades.txt");

    // Parse the file for the number of students
    inStream >> maxStudents;

    int testScores[maxStudents][numTests];
    int test, student, score, acrossAverage;

    // Walk through the rest of the file and store test data
    for (student = 0; student < maxStudents; student++)
    {
        for (test = 0; test < numTests; test++)
        {
            inStream >> score;
            testScores[student][test] = score; 
        }  
    }
    // Calculate and display the student average
    for (student = 0; student < maxStudents; student++)
    {
        cout << "Student " << (student+1) << ": ";
        cout << testAvg(testScores[student][0], 
            testScores[student][1], testScores[student][2],
            testScores[student][3]) << endl;
    }
    // Calculate and display the test average
    for (test = 0; test < numTests; test++)
        {
            cout << "Class Average Test " << (test+1) << ": ";
            cout << classAvg(maxStudents, testScores, test);
            cout << endl;
        }
    inStream.close();
    return 0;
}
