#include <iostream>

using namespace std;

int main()
{
    int x, y, sum;

    cin >> y;

    for (x = 0; x < y; x++)
    {
        if (x % 3 == 0 || x % 5 == 0)
            sum = sum + x;
    }
    cout << sum << endl;
    return 0;
}
