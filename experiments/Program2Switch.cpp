// Include the needed files/libraries
#include <iostream>

// Include the std namespace
using namespace std;

// In the main() do the following
int main() 
{
    // Define three variables, two for user input (distance in meters (int), menu choice (char),
    // and result (float))
    int meterInput;
    char userChoice;
    float distOutput;
    // Ask the user to enter the meters and read in their data
    cout << "Enter the distance in meters: ";
    cin >> meterInput;
    // If the input is zero or negative, print an error and exit the program
    if (meterInput <= 0)
    {
        cout << "ERROR: Input must not be zero or negative." << endl;
        exit(-1);
    }
    // Print a menu that asks for which conversion to apply
    cout << "C - Convert to centimeters" << endl;
    cout << "M - Convert to millimeters" << endl;
    cout << "K - Convert to kilometers" << endl;
    cout << "F - Convert to feet" << endl;
    cout << "Y - Convert to yards" << endl;
    cout << "I - Convert to miles" << endl;
    cout << "Your choice: ";
    cin >> userChoice;
    // If the user input is valid, apply the corresponding conversion and place answer in result
    switch (userChoice)
    {
        case 'C':
        case 'c':
            distOutput = meterInput * 100.0;
            break;
        case 'M':
        case 'm':
            distOutput = meterInput * 1000.0;
            break;
        case 'K':
        case 'k':
            distOutput = meterInput / 1000.0;
            break;
        case 'F':
        case 'f':
             distOutput = meterInput * 3.28084;
             break;
        case 'Y':
        case 'y':
             distOutput = meterInput * 1.09361;
             break;
        case 'I':
        case 'i':
             distOutput = meterInput / 1609.344;
             break;
        default:
             cout << "ERROR: Invalid menu option." << endl;
             exit(-1);
             break;
    }
    // Print out the result
    cout << "The conversion of " << meterInput << " meters is " << distOutput << endl;
    return 0;
}
