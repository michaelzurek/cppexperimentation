/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 121, Spring 2019         *
 * Assignment: Program 5                *
 * Due Date:  March 8th, 2019           *
 * Last Modified: March 7th 16:10       *
 ****************************************/

#include <iostream>
#include <fstream>
#include <string.h>
#include "student.h"

using namespace std;

// Average tests for a student
float testAvg(int testScores[])
{
    int total = 0;
    int test;
    for (test = 0; test < numTests; test++)
    {
        total = testScores[test] + total;
    }
    return(float(total/numTests));
}
// Average scores for a particular test across the class
// taking in the amount of students, the test scores, and quiz
float classAvg(int numStudents, students students[], int testNum)
{
    int student, sum = 0;
    for (student = 0; student < numStudents; student++)
    {
        sum = sum + students[student].testScores[testNum];
    }
    return(sum/(float) numStudents);
}

int main() 
{
    int test, student, score, numStudents;
    float tempAverage;
    char tempID[maxIDLength];
    students students[maxStudents];
    // Open the file that contains the grades
    ifstream inStream("prog5grades.txt");

    // Parse the file for the number of students
    inStream >> numStudents;

    // Walk through the rest of the file and store student data
    for (student = 0; student < numStudents; student++)
    {
        inStream >> tempID;
        strcpy(students[student].id, tempID);
        for (test = 0; test < numTests; test++)
        {
            inStream >> score;
            students[student].testScores[test] = score;
        }
    }
    // Calculate the student average
    for (student = 0; student < maxStudents; student++)
    {
        students[student].average = 
            testAvg(students[student].testScores);
    }
    // Display the student average
    for (student = 0; student < numStudents; student++)
    {
        cout << "Student " << students[student].id << ": ";
        cout << students[student].average << endl;
    }
    // Calculate and display the test average
    for (test = 0; test < numTests; test++)
        {
            cout << "Class Average Test " << (test+1) << ": ";
            cout << classAvg(numStudents, students, test) << endl;
        }
    inStream.close();
    return 0;
}
