/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 121, Spring 2019         *
 * Assignment: Program 5                *
 * Due Date:  March 8th, 2019           *
 * Last Modified: March 7th 16:10       *
 ****************************************/

const int maxIDLength = 10;
const int numTests = 5;
const int maxStudents = 24;

struct students
{
    char id[maxIDLength];
    int testScores[numTests];
    float average;
};
