/****************************************
 * Name: Michael Zurek                  *
 * Class: CSCI 121, Spring 2019         *
 * Assignment: Program2                 *
 * Due Date: January 29th, 2019         *
 * Last Modified: 15:16 January 22nd    *
 ****************************************/

// Include the needed files/libraries
#include <iostream>

// Include the std namespace
using namespace std;

// In the main() do the following
int main() 
{
    // Define three variables, two for user input (distance in meters (int), menu choice (char),
    // and result (float))
    int meterInput;
    char userChoice;
    float distOutput;
    // Ask the user to enter the meters and read in their data
    cout << "Enter the distance in meters: ";
    cin >> meterInput;
    // If the input is zero or negative, print an error and exit the program
    if (meterInput <= 0)
    {
        cout << "ERROR: Input must not be zero or negative." << endl;
        exit(-1);
    }
    // Print a menu that asks for which conversion to apply
    cout << "C - Convert to centimeters" << endl;
    cout << "M - Convert to millimeters" << endl;
    cout << "K - Convert to kilometers" << endl;
    cout << "F - Convert to feet" << endl;
    cout << "Y - Convert to yards" << endl;
    cout << "I - Convert to miles" << endl;
    cout << "Your choice: ";
    cin >> userChoice;
    // If the user input is valid, apply the corresponding conversion and place answer in result
    if (userChoice == 'C')
    {
        distOutput = meterInput * 100.0;
    }
    else if (userChoice == 'M')
    {
        distOutput = meterInput * 1000.0;
    }
    else if (userChoice == 'K')
    {
        distOutput = meterInput / 1000.0;
    }
    else if (userChoice == 'F')
    {
        distOutput = meterInput * 3.28084;
    }
    else if (userChoice == 'Y')
    {
        distOutput = meterInput * 1.09361;
    }
    else if (userChoice == 'I')
    {
        distOutput = meterInput / 1609.344;
    }
    // Otherwise print an error for incorrect menu selection and exit the program
    else
    {
        cout << "ERROR: Invalid menu option." << endl;
        exit(-1);
    }
    // Print out the result
    cout << "The conversion of " << meterInput << " meters is " << distOutput << endl;
    return 0;
}
